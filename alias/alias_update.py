#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os
import subprocess

home_dir = os.getcwd()
base_path = "/home/mfg"
# if running it directly from where alias.txt reside, use alias_output_path2
# otherwise for running it as initialize.py use alias_output_path
alias_output_path  = home_dir + "/alias/alias.txt"
alias_output_path2 = home_dir + "/alias.txt"
# change it from here
ALIAS_PATH = alias_output_path2

bash_rc_path = base_path + "/.bashrc"
bash_rc = ""
source_bash = "source ~/.bashrc"

def alias_command():
        if os.path.exists(ALIAS_PATH):
            with open(ALIAS_PATH, 'r') as f:
                readlines = f.read()
                bash_rc = readlines
                f.close()
            print("Successfully read %s" % ALIAS_PATH)
        else:
            print("Unable to locate alias_output.txt file in current directory")

        if os.path.exists(bash_rc_path):
            with open(bash_rc_path, "a") as f:
                f.write(bash_rc)
                f.close()
            print("Successfully updated bashrc")
        else:
            print("Creating bashrc unsuccessful")
	
	try:
	    subprocess.call(source_bash, shell=True)
	except OSError:
	    print("Bashrc refresh has failed")
	else:
	    print("Refreshed bashrc...")

if __name__ == '__main__':
        alias_command()
