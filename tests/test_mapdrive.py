#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os
import subprocess

mnt_path = "/mnt"
pc_path = "/vmwa3q003"

# /mnt/w
usershare_path = mnt_path + "/w"
# /mnt/vmwa3q0003 ex.
windows_path = mnt_path + pc_path
# //vmwa3q0003/workspaces
workspace_path = "/" + pc_path + "/workspaces"

USER_DIRECTORY = "sudo mkdir {0}".format(usershare_path)
PC_DIRECTORY = "sudo mkdir {0}".format(windows_path)

MOUNT_USER_DIRECTORY = "sudo mount cona05:/sbox014/sbox014/user/a3q {0}".format(usershare_path)
MOUNT_PC_DIRECTORY = "sudo mount -v -t cifs -o username=mfg,password=qad,uid=52140,gid=52140 {0} {1}".format(workspace_path, windows_path)

try:
	subprocess.call(USER_DIRECTORY, shell=True)
	subprocess.call(PC_DIRECTORY, shell=True)
except OSError:
        print("Failed to create directories: {0} and {1}".format(usershare_path, windows_path))
else:
        print("Successfully created directories: {0} and {1}".format(usershare_path, windows_path)) 

try:
	subprocess.call(MOUNT_USER_DIRECTORY, shell=True)
	subprocess.call(MOUNT_PC_DIRECTORY, shell=True)
except OSError:
	print("Error mouting drives")
else:
	print("Successfully mounted drives")

