#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os

home_dir = os.getcwd()
base_path = "/home/mfg"
alias_output_path = home_dir + "/alias.txt"
bash_rc_path = base_path + "/.bashrc"

if os.path.exists(alias_output_path):
    with open(alias_output_path, 'r') as f:
        readlines = f.read()        
	bash_rc = readlines 
	f.close()
    print("Successfully read %s" % alias_output_path)
else:
    print("Unable to locate alias_output.txt file in current directory")

if os.path.exists(bash_rc_path):
    with open(bash_rc_path, "a") as f:
        f.write(bash_rc)
        f.close()
    print("Successfully updated bashrc")
else:
    print("Creating bashrc unsuccessful")
