#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os
import subprocess

home_dir = os.getcwd()
base_path = "/home/mfg"
alias_output_path = home_dir + "\\alias_output.txt"
bash_rc_path = base_path + "/.bashrc"

if os.path.exists(alias_output_path):
    with open(alias_output_path, 'r') as f:
        readlines = f.readlines()
        f.close()
    print("Successfully read %s" % alias_output_path)
else:
    print("Unable to locate alias_output.txt file in current directory")

bash_rc = "alias s=\"cd /dr01/qadapps/systest\"\nalias c=\"cat\"\nalias l=\"ls -al\"\nalias udlc=\"unset DLC\"\nalias config=\"cd build/config\"\nalias tstart=\"yab tomcat-installation-service-start\"\nalias ystatus=\"yab status\"\nalias ytomcat=\"yab tomcat-webui-stop tomcat-webui-start\"\nalias tomcatstart=\"yab tomcat-webui-start\"\nalias tomcatstop=\"yab tomcat-webui-stop\"\nalias ysnapshot1=\"yab dev-data-snapshot -tag:snapshot1\"\nalias ysnapshot2=\"yab dev-data-snapshot -tag:snapshot2\"\nalias ydlist=\"yab dev-data-list\"\nalias yddiff=\"yab dev-data-diff -snapshot1:snapshot1 -snapshot2:snapshot2\"\nalias ydpatch=\"yab dev-data-patch -snapshot1:snapshot1 -snapshot2:snapshot2\"\nalias log=\"cd /dr01/qadapps/systest/build/logs/\"\nalias catlog=\"cd /dr01/qadapps/systest/servers/tomcat-webui/logs\"\nalias appstopstart=\"yab appserver-stop appserver-start\"\nalias vieam=\"vi as-eam.server.000001.log\"\nalias viqra=\"vi as-qra.server.000001.log\"\nalias vicat=\"vi catalina.out\"\nalias viyab=\"vi yab.log\"\nalias tailcat=\"tail -f -n 50 catalina.out\"\nalias taileam=\"tail -f -n 50 as-eam.server.000001.log\"\nalias vicon=\"vi /dr01/qadapps/systest/build/config/configuration.properties\""


if os.path.exists(bash_rc_path):
        with open(bash_rc_path, "a") as f:
                f.write(bash_rc)
                f.close()
        print("Successfully updated bashrc")
else:
    print("Creating bashrc unsuccessful")
