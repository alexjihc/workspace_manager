#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os
import subprocess

private_keys= "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA16r7yIiCopwQaI5ko/daTbqXbE1V0kMwc8XXxUAygwjUnKHMq360AckYNnEXxHKYHk2VAwfCUMo1MYo44IfWPtQeaUwoHVcQLClJ49grnOeUIdw/bE8B1OiUIv550yxzXVsVPxv8xhXcCbhsiT2zmU8F75ihzhG0SAFcMngC6cuz6gRrdu1w+JHS/Me63xszYAI6Ri6khyrOJ5nRtya8suJKLLZOtYfPccJoUcp647laJ5EPWGpZak40t0mr9kn3hrGMP+TxjJglP+L1OzucufFX/wc704sNWuOCfd74ueXHPjmOIWaZKdYz1eBqeA5uh6tmbXDKeCCDhFqUB1rM4w=="

mfg_path = "/home/mfg"
path = mfg_path + "/.ssh"
sshd_config_path = "/etc/ssh/sshd_config"
authorized_keys_path = path + "/authorized_keys"

folder_security_rights = 0o700
authorized_keys_rights = 0o600

def puttygen_command():
	try:
	        os.mkdir(path, folder_security_rights)
	except OSError:
	        print("Directory create failed: %s " % path)
	else:
	        print("Successfully created directory %s " % path)
	
	if not os.path.exists(authorized_keys_path):
	        with open(authorized_keys_path, 'w+') as f:
	                f.write(private_keys)
	                f.close()
			print("Authorized keys added")
	
	os.chmod(authorized_keys_path, authorized_keys_rights)
	os.chmod(mfg_path, folder_security_rights)
	
	subprocess.call(["sudo", "sed", "-i", 's/#RSAAuthentication yes/RSAAuthentication yes/', sshd_config_path])
	subprocess.call(["sudo", "sed", "-i", 's/#PubkeyAuthentication/PubkeyAuthentication/', sshd_config_path])
	subprocess.call(["sudo", "sed", "-i", 's/#AuthorizedKeysFile/AuthorizedKeysFile/', sshd_config_path])
	
	subprocess.call("sudo service sshd restart", shell=True)


if __name__ == '__main__':
        puttygen_command()				
