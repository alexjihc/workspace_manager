#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os

base_path = "/home/mfg"
vim_rc_path = base_path + "/.vimrc"
vim_rc = "color desert\nsyntax on"

def vimrc_command():
	if not os.path.exists(vim_rc_path):
		with open(vim_rc_path, 'w+') as f:
			f.write(vim_rc)
			f.close()
		print("Successfully created vimrc")
	else:
    		print("Unsuccessful creating vimrc")


if __name__ == '__main__':
        vimrc_command()
