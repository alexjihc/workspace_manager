#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os
import subprocess

base_path = "/dr01/qadapps/systest/"
environment_file = base_path + "config/qad.qra.core/config/environment.xml"

appserver_eam = "sudo iptables -I INPUT -p tcp -m tcp --dport 4099 -j ACCEPT" 
appserver_qra = "sudo iptables -I INPUT -p tcp -m tcp --dport 3099 -j ACCEPT"
iptable_save = "sudo service iptables save"

try:
	subprocess.call(["sudo", "sed", "-i", 's/DefaultAPICallTimeout Value="120"/DefaultAPICallTimeout Value="1600"/', environment_file])
except OSError:
	print("Update {0} failed".format(environment_file))
else:
	print("Environment.xml successfully updated")

try:
	subprocess.call(appserver_eam, shell=True)
	subprocess.call(appserver_qra, shell=True)
	subprocess.call(iptable_save, shell=True)
except OSError:
        print("Failed to update iptables")
else:
        print("Iptables successfully updated")

