#! /tech/python/centos-69/2.7.14/bin/python
# Author Alex Choi

import os, sys
import subprocess

base_path	 = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.curdir))
alias_path	 = base_path + '/alias'
mapdrive_path	 = base_path + '/mapdrive'
oedebug_path	 = base_path + '/oedebug'
puttygen_path	 = base_path + '/puttygen'
vimrc_path	 = base_path + '/vimrc'

sys.path.append(alias_path)
sys.path.append(mapdrive_path)
sys.path.append(oedebug_path)
sys.path.append(puttygen_path)
sys.path.append(vimrc_path)


from alias_update import alias_command
from mapdrive import mapdrive_command
from debugger import oedebug_command
from puttyGen import puttygen_command
from vimrc import vimrc_command


def main():
	print("****Initializing....****")
	alias_command()
	mapdrive_command()
	oedebug_command()
	puttygen_command()
	vimrc_command()


if __name__ == '__main__':
	main()
		
